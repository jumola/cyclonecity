<?php
return [
  'admin' => [
    'name' => 'Simon',
    'full_name' => 'Simon L',
    'email' => 'ritz@jumola.com',
    'code' => 'simon@1!'
  ],
  'app' => [
    'name' => 'Cleaning Supplies NT',
    'description' => ''
  ],
  'facebook' => [
    'api' => [
      'app_id' => "1424003184510328",
		 	'app_secret' => "e5075d1a4bf3e651b853e4871f9f9f24",
	  	'default_graph_version' => 'v2.5'
    ],
    'scope' => ['email']
  ],
  'woo' => [
    'store' => 'http://cleaningsuppliesnt.com',
    'key' => 'ck_9ae8f9796ff21321493649db942b44b3d43b56c7',
    'secret' => 'cs_58ae5e95f3411d35599c3927e7e72e256bb201b5',
    'version' => [
        'wp_api' => true, // Enable the WP REST API integration
        'version' => 'wc/v2' // WooCommerce WP REST API version
    ]
  ]
];
