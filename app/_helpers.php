<?php

function altStrings($string){
  $arr = [];
  $arr['user'] = 'client';
  $arr['users'] = 'clients';
  return isset($arr[$string]) ? $arr[$string] : $string;
}// altNames

function getEnumValues($table,$field){
    $type = DB::select( DB::raw('SHOW COLUMNS FROM '.$table.' WHERE Field = "'.$field.'"') )[0]->Type;
    preg_match('/^enum\((.*)\)$/', $type, $matches);
    $enum = array();
    foreach(explode(',', $matches[1]) as $value){
        $v = trim( $value, "'" );
        $enum[] = $v;
    }
    return $enum;
}//getEnumValues

 function getSelectOptions($table,$request = array() ){
		$q = \DB::table($table);
		foreach( $request as $key => $value ){
			$q = $q->where($key,$value);
		}
		return $q->get();
}// getSelectOptions
