<?php

namespace App\Http\Controllers;

use App\Core;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Facebook;
use Intervention\Image\ImageManagerStatic as Image;

class ViewController extends Controller
{
    public function view(Request $request)
    {
      // get URL segment. default to index if none
      $view = $request->segment(1);
      $page = $view ? $view : "index";
  		$user = session('user');

      // page swich
  		switch($page)
      {
  			case "index":
          return redirect('admin/panel');
  				break;
  			case "logout":
  				session()->forget('user');
          return redirect('');
  				break;
  			default:
  		}//switch

  		$view_data['user'] = $user;
  		$view_data['page'] = $page;
      return view($page,$view_data);
    }// view

    public function loginViaFacebook(Request $request){
  		//Use this as callback URL, send user back to this URL
      session()->forget('app-fb-login-callback');
  		if( $request->input('return-url') )
  			session()->put("app-fb-login-callback", $request->input('return-url') );

  		$fb = new Facebook\Facebook( config('config.facebook.api') );
  		$helper = $fb->getRedirectLoginHelper();
  		$oauth_url = $helper->getLoginUrl( url("login-facebook-cb"), config('config.facebook.scope') );
  		return redirect($oauth_url);
  	}//loginViaFacebook

    public function loginViaFacebookCb(Request $request){
  			//Check if denied, ask him again!
  			if( $request->input("error") == "access_denied" )
          return redirect('login-facebook');

  			$token = Core::fbCodeToToken( $request->input("code") );
  			$extended_token = Core::fbTokenExtend($token);

  			$graphData = Core::fbGraphRequest(array(
  				"token" => $extended_token,
  				"endpoint" => "/me?fields=id,first_name,last_name,name,email,gender",
  				"params" => array()
  			));

  			$user['facebook_id'] 	= $graphData['id'];
  			$user['facebook_token'] = $extended_token;
  			$user['first_name'] = $graphData['first_name'];
  			$user['last_name'] = $graphData['last_name'];
        $user['name'] = $graphData['name'];
  			$user['email'] = isset($graphData['email']) ? $graphData['email'] : NULL;
  			$user['gender'] = isset($graphData['gender']) ? $graphData['gender'] : NULL;

  			$target = NULL;
  			$checkFbId = Core::by('User',array(
          "facebook_id" => $user['facebook_id']
        ))->first();

  			 if( count($checkFbId) ){
  			 	 	$checkFbId->facebook_token = $user['facebook_token'];
  				 	$checkFbId->save();
  			 		$target = $checkFbId;
  			 }

  			 if(!$target){
  			 	//Set username and
  			 	$user['username'] = User::generateUsername( $user['first_name'] . $user['last_name'] );
  			 	$target = Core::add(new User(),$user);
  				 //Get - set profile picture
  				 Core::saveImage($target->id,"user","https://graph.facebook.com/".$target->facebook_id."/picture?type=square&width=250&height=250&access_token=".$target->facebook_token);
  			 }

  			//Put as seession
  			User::login($target->id);

        $sessionCallback = session()->get('app-fb-login-callback');
  			$to = $sessionCallback ? $sessionCallback : url("");
  			return redirect( $to );
  	}//login_facebook_cb

    public function importer(Request $request)
    {
      $items = \DB::table('tbl_user')->get();

      foreach( $items as $item ){
        Core::add('User',[
          'id' => $item->user_id,
          'name' => $item->person_name,
          'job_title' => $item->job_title,
          'business_name' => $item->business_name,
          'work_phone' => $item->work_phone,
          'mobile_phone' => $item->mobile_phone,
          'email' => $item->email_address,
          'referrer' => $item->reffered_by,
          'address1' => $item->address1,
          'address2' => $item->address2,
          'suburb' => $item->suburl,
          'state' => $item->state,
          'postal' => $item->postalcode,
          'contacttype_id' => $item->contact_type,
          'rating' => $item->rating,
          'group_type' => $item->group_type,
          'profile_image' => $item->profile_image,
          'user_level' => $item->user_level
        ]);
      }

      return count($items);
    }// importer


}
