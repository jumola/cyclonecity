<?php
namespace App\Http\Controllers;

use App\Admin;
use App\Core;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\helpers;

use App\Mail\PasswordReminder;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller {

	public $tableInputs = [
		'order' => [
			'status' => [
				'select' => ['pending','completed','cancelled','refunded']
			]
		],
		'product' => [
			'catalog_visibility' => [
				'select' => ['visible','catalog','search','hidden']
			],
			'description' => [
				'editor' => null
			],
			'featured' => [
				'select' => [0,1]
			],
		],
		'user' => [
			'tier' => [
				'select' => 'enum'
			]
		],
		'admin' => [
			'super' => [
				'select' => 'enum'
			]
		],

	];

	public function admin(Request $request){
		$view_data['config'] = config("config");
		$view_data['_request'] = $request;
		if(session()->get("admin.in")){
			$admin = session()->get("_admin");
			$view_data['adminStats'] = Admin::stats();
			$view_data['tables'] = Core::Tables();
			$view_data['admin'] = session()->get("_admin");;
			return view("admin/view/dashboard",$view_data);
		}else{
			session()->forget("admin");
			return view("admin/login",$view_data);
		}
	}//admin

	public function custom(Request $request){
		$admin = session()->get("_admin");
		$view_data['config'] = config("config");
		if( !session()->get("admin.in") )
			return redirect('admin/panel');

		$page = $request->segment(4);
		$view_data['tables'] = Core::Tables();

		if( $page == "learning-manager" ){
			$view_data['courses'] = Core::by('Course',[])->get();
		}elseif( $page == 'lesson-manager' ){
			$course_id = $request->input('course_id');
			$view_data['course'] = Core::by('Course',['id'=>$course_id])->first();
		}elseif( $page == 'subject-manager' ){
			$lesson_id = $request->input('lesson_id');
			$view_data['lesson'] = Core::by('Lesson',['id'=>$lesson_id])->first();
		}
		$view_data['admin'] = session()->get("_admin");
		return view("admin.view.custom.".$page,$view_data);
	}//custom



	public function login(Request $request){
		$params = $request->all();
		$admin = Core::by('Admin',$params)->first();
		if( $admin ){
			session()->put("admin.in",true);
			session()->put("_admin",$admin);
		}else{
			session()->forget("admin.in");
			session()->forget("_admin");
		}
		return redirect("admin/panel");
	}//login

	public function logout(){
		session()->forget('admin');
		return redirect("admin/panel");
	}//logout

	public function passwordReminder(Request $request){

		$params = $request->all();
		$admin = Core::by('Admin',$params)->first();

		if( $admin ){
			Mail::to($admin->email)->
	    send(new PasswordReminder([
	      'admin' => $admin
	    ]));
		}

		return back()->with('success',"If there's an account with these credentials, you will receive an email to recover your password.");
	}// passwordReminder

	public function reset_app(){
		$tables = Core::tables();
		foreach($tables as $table){
			\DB::table($table)->truncate();
		}
		return redirect("admin/panel");
	}//reset_app

	public function saveProductPricing(Request $request){
		$params = $request->all();

		$finalParams = [];
		foreach( $params as $col =>  $param ){
			if( $param && is_numeric($param) ){
				$finalParams[$col] = $param;
			}
		}

		$updated = Core::updates($params['id'],'Product',$finalParams);
		$updated->updateWoo($updated);
	}// saveProductPricing

	public function validateCreate($table,$request)
	{
		$rules = [];
		// course
		$rules['course'] = [
			'name' => 'required',
			'description' => 'required',
			'code' => 'required',
			'active' => 'required|numeric',
			'image' => 'image'
		];
		// lesson
		$rules['lesson'] = [
			'name' => 'required',
			'order' => 'required|numeric',
			'course_id' => 'required|exists:course,id'
		];
		// subject
		$rules['subject'] = [
			'order' => 'required|numeric',
			'lesson_id' => 'required|exists:lesson,id',
			'title' => 'required',
			'details' => 'required',
			'locked' => 'required|numeric'
		];

		// if no rules set
		if( !isset($rules[$table]) )
			return null;

		$validator = \Validator::make($request,$rules[$table]);

		return $validator->fails() ? Core::validator_error($validator,true) : NULL;
	}

	public function validateEdit($table,$request)
	{
		$rules = [];
		// course
		$rules['course'] = [
			'name' => 'required',
			'description' => 'required',
			'code' => 'required',
			'active' => 'required|numeric',
			'image' => 'image'
		];
		// lesson
		$rules['lesson'] = [
			'name' => 'required',
			'order' => 'required|numeric',
			'course_id' => 'required|exists:course,id'
		];
		// subject
		$rules['subject'] = [
			'order' => 'required|numeric',
			'lesson_id' => 'required|exists:lesson,id',
			'title' => 'required',
			'details' => 'required',
			'locked' => 'required|numeric'
		];

		// if no rules set
		if( !isset($rules[$table]) )
			return null;

		$validator = \Validator::make($request,$rules[$table]);

		return $validator->fails() ? Core::validator_error($validator,true) : NULL;
	}

	public function view(Request $request){
		$table 	= request()->segment(4);
		$type 	= request()->segment(5);
		$id 		= $request->input("id");

		switch($type){
			case 'add':
				$view_data = $this->viewTableAdd($table,$request);
				break;
			case 'all':

				$customs = [
					'course' => 'admin/panel/custom/learning-manager',
					'lesson' => 'admin/panel/custom/learning-manager',
					'subject' => 'admin/panel/custom/subject-manager?lesson_id='
				];

				if( isset($customs[$table]) ){
					if( $table == 'subject' ){
						$subject = Core::by('Subject',['id'=>$request->input('id')])->first();
						$customs[$table] .= $subject->lesson_id;
					}

					return redirect($customs[$table]);
				}else{
					$view_data = $this->viewTableAll($table,$request);
				}
				break;
			case "create":

				$Table = ucfirst($table);

				// creation validation
				$invalid = $this->validateCreate($table,$request->all());
				if( $invalid )
					return back()->withInput($request->except('image'))->with('error',$invalid);

				//Add
				$new = Core::add($Table, $request->all() );


				if( $request->hasFile('local_image') )
					Core::saveImage($new->id, $table, $request->file('local_image') );


				// if( $table == "course" ){
				// 	// save image
				// 	if( $request->hasFile('image') )
				// 		Core::saveImage($new->id,$table, $request->file('image') );
				// }

				return redirect('admin/panel/view/'. $table .'/all');
				break;
			case "delete":
				$delete 	= \DB::table($table)->where("id",$id)->delete();
				return redirect()->back();
				break;
			case 'edit':
				$view_data = $this->viewTableEdit($table,$request);

				break;
			case "update":

				//$id,$table,$request
				$Table = ucfirst($table);

				// creation validation
				$invalid = $this->validateEdit($table,$request->all());
				if( $invalid )
					return back()->withInput($request->except('image'))->with('error',$invalid);



				$updated = Core::updates($id,$Table,$request->all());

				if( $table == "product" ){
					$updated->updateWoo($updated);
				}

				return redirect()->back()->with("success","Updated!");
				break;
		}

		$view_data['id'] 		= $id;
		$view_data['table'] 	= $table;
		$view_data['config'] 	= config("config");
		$view_data['tables'] = Core::Tables();
		$view_data['admin'] = session()->get("_admin");
		$view_data['_request'] = $request;
		return view("admin/view/table/". $type,$view_data);
	}//view

	public function viewTableAdd($tableName,$request){
		$columns = \Schema::getColumnListing($tableName);

		$adds['destination'] = ['local_image'];

		$removes[$tableName] = ['id','created_at','updated_at'];


		$inputs['destination']['local_image'] = ["file" => NULL ];
		$inputs['promo']['destination_id'] = ['select' => 'destination' ];
		$inputs['promo']['travel_from'] = ['date' => NULL ];
		$inputs['promo']['travel_to'] = ['date' => NULL ];
		$inputs['promo']['expires_at'] = ['date' => NULL ];

		$column_removes = isset($removes[$tableName]) ? $removes[$tableName] : array();
		$column_adds = isset($adds[$tableName]) ? $adds[$tableName] : array();

		$columns = Admin::processColumns($columns,$column_removes,$column_adds);

		if($tableName == 'subject'){
			$view_data['lesson'] = Core::by('Lesson',['id'=>$request['lesson_id']])->first();
		}

		$view_data['params'] = $request;
		$view_data['inputs'] = $inputs;
		$view_data['columns'] = $columns;
		$view_data['inputs'] = $this->tableInputs;
		$view_data['admin'] = session()->get("_admin");
		return $view_data;
	}

	public function viewTableAll($tableName,$request){
		$columns =  \Schema::getColumnListing($tableName);

		$removes = Core::Tables(array("migrations"),true);
		$removes['user'] = array('facebook_token','tier');
		$removes['product'] = ['id','created_at','price','updated_at','image','permalink','sku','description'];
		$removes['quote_item'] = ['id','created_at','updated_at'];

		$view_data['columns'] = Admin::processColumns($columns,$removes[$tableName]);
		$view_data['getData'] = $getData = \DB::table($tableName)->get();
		$view_data['admin'] = session()->get("_admin");
		return $view_data;
	}

	public function viewTableEdit($tableName,$request){
		$columns = \Schema::getColumnListing( $tableName );

		$adds['course'] = array('image');

		$removes[$tableName] = ['id','created_at','updated_at'];

		$removes['course'] = ['id','created_at','updated_at'];
		$removes['lesson'] = ['id','created_at','updated_at'];
		$removes['product'] = ['id','created_at','updated_at','price','image','permalink','sku'];

		// $inputs['course']["active"] = array("select" => [1,0] );
		//

		//
		// $inputs['subject']["lesson_id"] = ["select" => 'lesson'];
		// $inputs['subject']["details"] = ["editor" => null ];
		// $inputs['subject']["editor"] = ["codeEditor" => null ];
		// $inputs['subject']["answer"] = ["codeEditor" => null ];
		// $inputs['subject']["locked"] = ["select" => [1,0] ];

		$column_removes = isset($removes[$tableName]) ? $removes[$tableName] : array();
		$column_adds = isset($adds[$tableName]) ? $adds[$tableName] : array();

		$columns = Admin::processColumns($columns,$column_removes,$column_adds);

		$target = $tableName;
		$Model = ucfirst($tableName);

		$target = Core::by($Model,array(
			'id'=> $request->input('id')
		))->first();


		// side data
		if( $tableName == 'order' ){
			$view_data['orderProducts'] = Core::by('Orderproduct',[
				'order_id' => $target->id
			])->get();
		}


		$view_data['target'] = $target;
		$view_data['inputs'] = $this->tableInputs;
		$view_data['columns'] = $columns;
		$view_data['admin'] = session()->get("_admin");
		return $view_data;
	}

}//AdminController
