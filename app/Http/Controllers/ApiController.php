<?php

namespace App\Http\Controllers;

use App\Core;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Facebook;
use Intervention\Image\ImageManagerStatic as Image;

use Automattic\WooCommerce\Client;
use Automattic\WooCommerce\HttpClient\HttpClientException;

class ApiController extends Controller
{

  public function saveUserTier(Request $request){
    $params = $request->all();

    $user = \App\User::find($params['id']);
    $user->tier = $params['tier'];
    $user->save();

    echo true;
  }// saveUserTier

    public function get(Request $request){
      $table = $request->segment(2);
      $Table = ucfirst($table);

      $items = Core::by($Table,$request->all())->get();

      $items = $this->getPostProcess($table,$items);

      return \Response::json([
        'status' => true,
        'data' => $items,
        'message' => "Data pulled"
      ]);
    }//get

    public function hook(Request $request){
      $action = $request->segment(3);

      $params = $request->all();
      switch( $action ){
        case 'user-create':
            $this->hookUserCreate($params);
          break;
        case 'order-create':
            $this->hookOrderCreate($params);
          break;
        case 'refresh-products':
           $this->refreshProducts();
           break;
        case 'product-create':
            $this->hookProductCreate($params);
          break;
        case 'product-delete':
            $this->hookProductDelete($params);
          break;
        case 'tiered-price':
           $this->getTieredPrice($params);
           break;
        case 'product-update':
           $this->hookerProductUpdate($params);
           break;
      }
    }// hook

    public function getTieredPrice($params){
      // // user_id | product_id
      // // check if product exists
      $product = Core::by('Product',[
        'wp_id' => $params['product_id']
      ])->first();

      // check if user exists
      $user = Core::by('User',[
        'wp_id' => $params['user_id']
      ])->first();

      if( !$product || !$user )
         return NULL;

      echo $tier = $user->tier == 'default' ? $product->regular_price : $product[$user->tier];exit;
    }// getTieredPrice

    public function hookOrderCreate($data){
      \Log::info('hookOrderCreate');
      \Log::info(json_encode($data));

      if( !isset($data['customer_id']) ){
        return false;
      }

      try{
        // find user by wp_user_id first to get his local id
        $user = Core::by('User',[
          'wp_id' => $data['customer_id']
        ])->first();

        if( !$user ){
          $user = Core::by('User')->
          where('email',$billing['email'])->
          first();
        }

        // if user doesn't exist, create one. use billing data.
        if( !$user ){
          $billing = $data['billing'];
          $user = Core::add('User',[
            'name' => $billing['first_name'] . ' ' . $billing['last_name'],
            'email' => $billing['email'],
            'address1' => $billing['address_1'],
            'address2' => $billing['address_2'],
            'city' => $billing['city'],
            'state' => $billing['state'],
            'postal' => $billing['postcode'],
            'mobile_phone' => $billing['phone'],
          ]);

          \Log::info('new user');
          \Log::info(json_encode($user));
        }else{

                  \Log::info('with user');
                  \Log::info(json_encode($user));
        }

        // now, insert order
        $order = Core::add('Order',[
          'user_id' => $user->id,
          'wp_id' => $data['id'], // order id
          'wp_customer_id' => $data['customer_id'],
          'total' => $data['total'],
          'currency' => $data['currency'],
          'customer_note' => $data['customer_note'],
          'payment_method_title' => $data['payment_method_title']
        ]);

        \Log::info('order');
        \Log::info(json_encode($order));

        // now create and relate products to this order
        $products = $data['line_items'];
        foreach( $products as $product ){

          \Log::info('adding');
          \Log::info(json_encode($product));


          try{

            // check if this product already exists (as we need local product id)
            //TODO
            $prod = Core::by('Product',[
              'wp_id' => $product['product_id']
            ])->first();

            if( !$prod )
              $prod = Core::add('Product',[
                'wp_id' => $product['product_id'],
                'name' => $product['name'],
              ]);

            //TODO pull this newly locally added product

            // add
            Core::add('Orderproduct',[
              'order_id' => $order->id,
              'product_id' => $product['product_id'], // wp product id
              'wp_id' => $product['id'], // wp product id
              'quantity' => $product['quantity'],
              'total' => $product['total'],
            ]);

          }catch(Exception $e){
            \Log::error('hookOrderCreate');
            \Log::error($e->getMessage());
          }

        }
      }catch(Exception $e){
        return 'error';
        \Log::error('hookOrderCreate');
        \Log::error($e->getMessage());
      }

    }// hookOrderCreate

    public function hookProductCreate($data){
      \Log::info('hookProductCreate');
      \Log::info(json_encode($data));

      $target = Core::by('Product',[
        'wp_id' => $data['id']
      ])->first();

      if( $target ){
        $this->hookerProductUpdate($data);
      }else{
        // insert new product
        Core::add('Product',[
          'wp_id' => $data['id'], // product id
          'name' => $data['name'],
          'description' => $data['description'],
          'short_description' => $data['short_description'],
          'price' => isset($data['price']) && $data['price'] ? $data['price'] : NULL,
          'regular_price' => isset($data['regular_price']) && $data['regular_price'] ? $data['regular_price'] : NULL,
          'sale_price' => isset($data['sale_price']) && $data['sale_price'] ? $data['sale_price'] : NULL,
          'image' => $data['images'][0]['src'],
          'catalog_visibility' => $data['catalog_visibility'],
          'sku' => $data['sku'],
          'featured' => $data['featured'],
        ]);
      }

    }// hookerProductCreate

    public function hookProductDelete($data){
      \Log::info('hookProductDelete');
      \Log::info(json_encode($data));
      // delete product
      Core::by('Product',[
        'wp_id' => $data['id'], // product id
      ])->delete();
    }// hookProductDelete

    public function hookerProductUpdate($data){
      \Log::info('hookerProductUpdate');
      \Log::info(json_encode($data));
      //

      $target = Core::by('Product',[
        'wp_id' => $data['id']
      ])->first();

      if( $target ){
        Core::updates($target->id,'Product',[
          'name' => $data['name'],
          'description' => $data['description'],
          'short_description' => $data['short_description'],
          'price' => isset($data['price']) && $data['price'] ? $data['price'] : NULL,
          'regular_price' => isset($data['regular_price']) && $data['regular_price'] ? $data['regular_price'] : NULL,
          'sale_price' => isset($data['sale_price']) && $data['sale_price'] ? $data['sale_price'] : NULL,
          'image' => $data['images'][0]['src'],
          'catalog_visibility' => $data['catalog_visibility'],
          'sku' => $data['sku'],
          'featured' => $data['featured'],
        ]);
      }else{
        $this->hookProductCreate($data);
      }
    }// hookerProductUpdate

    public function hookUserCreate($params){
      // make sure user doesn't exist yet (by email)
      $user = Core::by('User',[
        'email' => $params['email']
      ])->count();

      if(!$user)
        Core::add('User',[
          'wp_id' => $params['id'],
          'name' => $params['first_name'] . ' ' . $params['last_name'],
          'email' => $params['email']
        ]);
    }// hookUserCreate

    public function refreshProducts(){




      try {

        ini_set('max_execution_time', 180); //3 minutes
        set_time_limit(0);

        $woocommerce = new Client(config('config.woo.store'),config('config.woo.key'),config('config.woo.secret'),config('config.woo.version'));


        $products = $woocommerce->get('products');

        foreach( $products as $data ){

          $target = Core::by('Product',[
            'wp_id' => $data['id']
          ])->first();

          $datas = [
            'wp_id' => $data['id'],
            'name' => $data['name'],
            'description' => $data['description'],
            'short_description' => $data['short_description'],
            'price' => isset($data['price']) && $data['price'] ? $data['price'] : NULL,
            'regular_price' => isset($data['regular_price']) && $data['regular_price'] ? $data['regular_price'] : NULL,
            'sale_price' => isset($data['sale_price']) && $data['sale_price'] ? $data['sale_price'] : NULL,
            'image' => $data['images'][0]['src'],
            'catalog_visibility' => $data['catalog_visibility'],
            'sku' => $data['sku'],
            'featured' => $data['featured'],
          ];

          if( $target ){
            Core::updates($target->id,'Product',$datas);
          }else{
            Core::add('Product',$datas);
          }

        }

        echo count($products); exit;


} catch (HttpClientException $e) {
    echo $e->getMessage(); exit;
}

    }// refreshProducts





}
