<?php
namespace App;

use App\User;
use Facebook;
use Illuminate\Database\Eloquent\Model;

use Intervention\Image\ImageManagerStatic as Image;

class Core extends Model {

	public static function add($Model,$request){
		$modelName = 'App\\'.$Model;
		$model = new $modelName;

		$columns = \Schema::getColumnListing( $model::table() );
		foreach($request as $field => $value){
			if(in_array($field, $columns)){
				$model->$field = $value;
			}
		}// foreach

		$model->save();
		return $model;
	}//add

	public static function by($Class,$request = array()){
		$modelName = 'App\\'.$Class;
		$Class = new $modelName;
		$table_name = $Class::table();
		$columns = \Schema::getColumnListing($table_name);
		foreach($request as $field => $value){
			if( in_array($field, $columns) ){
				$Class = $Class->where($table_name.".".$field,"=",$value);
			}
		}//foreach
		return $Class;
	}//by

	public static function curl($url){
		$ch = curl_init();
	  	curl_setopt($ch, CURLOPT_URL,$url);
	  	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	  	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	  	$api_result = curl_exec($ch);

		$api_result = str_replace("({", "{", $api_result);
		$api_result = str_replace("});", "}", $api_result);

	  	return json_decode($api_result, true); // decode the data into a usable php array
	}//curl

	public static function fbCodeToToken($code){
		$getTokenUrl 	= "https://graph.facebook.com/oauth/access_token?";
		$getTokenUrl	.= "client_id=". config('config.facebook.api.app_id');
		$getTokenUrl	.= "&redirect_uri=".url("login-facebook-cb");
		$getTokenUrl	.= "&client_secret=". config('config.facebook.api.app_secret');
		$getTokenUrl	.= "&code=".$code;
		$getToken 		= @file_get_contents($getTokenUrl);
		$getToken 		= explode("&",$getToken);

		if( !isset($getToken[0]) )
			return false;

		$tmp = json_decode($getToken[0]);
		return isset($tmp->access_token) ? $tmp->access_token : false;
	}//fbCodeToToken

	public static function fbGraphRequest($request,$method = "GET"){
		try {
			$fb = new Facebook\Facebook( config('config.facebook.api') );
			$response = $fb->setDefaultAccessToken( $request['token'] );
			if( $method == "GET" ){
				$response = $fb->get($request['endpoint']);
			}elseif( $method == "POST" ){
				$params = isset($request['params']) ? $request['params'] : array();
				$response = $fb->post( $request['endpoint'], $params);
			}
			return $response->getDecodedBody();
		}catch(Exception $e){
		  // When Graph returns an error
		  Log::error($e->getMessage());
		  return false;
		}
	}//fbGraphRequest

	public static function fbTokenExtend($short_lived_token){
		try{

			$fb = new Facebook\Facebook( config('config.facebook.api') );

			// OAuth 2.0 client handler
			$oAuth2Client = $fb->getOAuth2Client();

			// Exchanges a short-lived access token for a long-lived one
			$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken( $short_lived_token );
			return $longLivedAccessToken->getValue();
		}catch(Exception $e){
			return false;
		}
	}//fbTokenExtend

	public static function getImage($id,$folder,$absolute = true){
		$filename = $id . ".png";
		$destination = "assets/" . $folder;
		$file = $destination . "/" . $filename;
		$temp_file = "assets/".$folder."/placeholder.jpg";
		return \File::exists(public_path($file)) ? ( $absolute ? asset($file) : public_path($file) ) : ( $absolute ? asset($temp_file) : public_path($temp_file) );
	}//getImage

	public static function ifThen($what,$should,$return = true,$else = false){
		return $what == $should ? $return : $else;
	}//ifThen

	public static function saveImage($id,$folder,$source, $width = NULL, $height = NULL ){
		try{
			$filename = $id . ".png";
			$destination = "assets/" . $folder;
			if( $width || $height ){
				$resized = \Image::make( $source )->encode("png");
				if( $width && $resized->width() > $width ){
					$resized->resize($width, null, function ($constraint) {
					    $constraint->aspectRatio();
					});
				}
				if( $height && $resized->height() > $height ){
					$resized->resize(null, $height, function ($constraint) {
					    $constraint->aspectRatio();
					});
				}
				$resized->save( public_path( $destination . "/" . $filename ) );
			}else{
				\Image::make( $source )->encode("png")->save( public_path( $destination . "/" . $filename ) );
			}
		}catch(Exception $e){
			Log::error($e->getMessage() );
		}
	}//saveImage

	public static function tables($removes = array("migrations"),$associative = false){
		$tables = array();
		$raw = \DB::select('SHOW TABLES');
		foreach($raw as $item){
			$item = (array) $item;
			$key = key($item);
			if(!in_array($item[$key], $removes)){
				if($associative){
					$tables[$item[$key]] = array();
				}else{
					array_push($tables,$item[$key]);
				}
			}
		}
		return $tables;
	}//tables

	public static function timeAgo($created_time,$future_time = false,$type = NULL){
		$str = strtotime($created_time);
		$today = $future_time ? strtotime($future_time) : strtotime(date('Y-m-d H:i:s'));

        $time_differnce = $today-$str;// It returns the time difference in Seconds...
        $years 	= 60*60*24*365;// To Calculate the time difference in Years...
		$months = 60*60*24*30;// To Calculate the time difference in Months...
		$days 	= 60*60*24;// To Calculate the time difference in Days...
		$hours 	= 60*60;// To Calculate the time difference in Hours...
        $minutes = 60;// To Calculate the time difference in Minutes...

		switch($type){
        		case "days":
				return intval(($time_differnce/$days));
				break;
			case "minutes":
				return intval($time_differnce/$minutes);
				break;
			case "seconds":
				return intval(($time_differnce));
				break;
			default://timestamp
				if(intval($time_differnce/$years) > 1){
		            return intval($time_differnce/$years)." years";
		        }else if(intval($time_differnce/$years) > 0){
		            return intval($time_differnce/$years)." year";
		        }else if(intval($time_differnce/$months) > 1){
		            return intval($time_differnce/$months)." months";
		        }else if(intval(($time_differnce/$months)) > 0){
		            return intval(($time_differnce/$months))." month";
		        }else if(intval(($time_differnce/$days)) > 1){
		            return intval(($time_differnce/$days))." days";
		        }else if (intval(($time_differnce/$days)) > 0){
		            return intval(($time_differnce/$days))." day";
		        }else if (intval(($time_differnce/$hours)) > 1){
		            return intval(($time_differnce/$hours))." hours";
		        }else if (intval(($time_differnce/$hours)) > 0){
		            return intval(($time_differnce/$hours))." hour";
		        }else if (intval(($time_differnce/$minutes)) > 1){
		            return intval(($time_differnce/$minutes))." mins";
		        }else if (intval(($time_differnce/$minutes)) > 0){
		            return intval(($time_differnce/$minutes))." min";
		        }else if (intval(($time_differnce)) > 1){
		            return intval(($time_differnce))." secs";
		        }else{
		            return "few seconds";
		        }
		}//switch
	}//timeAgo

	public static function updates($id,$Class,$request){
		$modelName = 'App\\'.$Class;
		$Model = new $modelName;

		$target = Core::by($Class,[
			'id' => $id
		])->first();

		$columns = \Schema::getColumnListing($Model::table());
		foreach($request as $field => $value){
			if ( $field != "id" && in_array($field, $columns)) {
				$target->$field = $value;
			}
		}// foreach

		$target->save();
		return $target;
	}//updates

	public static function validator_error($validator,$combined = false){
		$final_error 	= "";
		$error_msg 		= array();
		$errors = $validator->messages()->toArray();
		foreach($errors as $field => $value){
			if($combined){
				$final_error .= $value[0] . "<br />";
			}else{
				$error_msg[$field] = $value[0];
			}
		}
		return $combined ? $final_error : $error_msg;
	}//validator_error

}//Core
