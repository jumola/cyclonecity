<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Product extends Model {
	public $table = "product";
	public static function table(){
		return "product";
	}//table

	public static function rules($for = NULL,$request = NULL){
		$rules = array();
		switch($for){
			case "xxx":

				break;
		}
		return $rules;
	}//rules

	public static function updateWoo($updated){
		$woocommerce = new \Automattic\WooCommerce\Client(config('config.woo.store'),config('config.woo.key'),config('config.woo.secret'),config('config.woo.version'));
		$alloweds = ['name','description','short_description','regular_price','sale_price'];
		$wooParams = [];
		foreach($alloweds as $col ){
			$wooParams[$col] = $updated->$col;
		}
		$woocommerce->put('products/'.$updated->wp_id,$wooParams);
	}

}//
