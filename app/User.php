<?php

namespace App;
use Facebook;
use Illuminate\Database\Eloquent\Model;

class User extends Model {
	public $table = "user";
	public static function table(){
		return "user";
	}//table

	public static function fbCheckPermissions($token){
		$request['token'] 		= $token;
		$request['endpoint'] 	= "/me/permissions";
		$request['params'] 		= NULL;
		$permissions = Core::fbGraphRequest($request);

		if( is_array($permissions) && count($permissions) ){

			$current_permissions = array();
			foreach($permissions as $permission){
				if($permission->status == "granted"){
					array_push($current_permissions,$permission->permission);
				}
			}

			$must_have_permissions = explode(",",Config::get("config.facebook.scope"));
			foreach($must_have_permissions as $mh_perm){
				if( !in_array($mh_perm, $current_permissions) ){
					return false;
				}
			}
			return true;
		}else{
			return false;
		}
	}//fbCheckPermissions

	public static function generateUsername($name){
		$name = str_replace("-", "", str_slug($name) );
		$max_chars = 10;
		$max_name_len = 7;
		$name = str_limit($name,$max_name_len,"");
		$tmp_username = $name . str_random($max_chars);
		return str_limit($tmp_username,$max_chars,"");
	}//generateUsername

	public static function login($id){
		$user = User::find($id);
		session()->put("user",$user);
		return $user;
	}//login


	public static function rules($for = NULL,$request = NULL){
		$user = session()->get("user");
		$rules = array();
		switch($for){
			case "saveProfile":
				$rules['first_name'] = 'required';
				$rules['last_name'] = 'required';
				$rules['username'] = 'required|alpha_num|min:8|max:15|unique:user,username,'. $user->id ;
				$rules['email'] = 'required|email|unique:user,email,'. $user->id ;
				$rules['password'] = 'confirmed|min:10';
				break;
		}
		return $rules;
	}//rules

}//User
