<?php
namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model {

	public $table = "admin";
	public static function table(){
		return "admin";
	}//table

	public static function rules($for = NULL,$request = NULL){
		$rules = array();
		switch($for){
			case "xxx":

				break;
		}
		return $rules;
	}//rules

	public static function processColumns($columns,$removes = array(),$adds = array()){
		//Removes
		$final = array();
		if(count($removes)){
			foreach($columns as $column){
				if(!in_array($column,$removes)){
					array_push($final,$column);
				}
			}
		}else{
			$final = $columns;
		}
		//Adds
		return count($adds) ? array_merge($final,$adds) : $final;
	}//processColumns

	public static function stats(){
		$date = new \DateTime();
		$today = $date->format("Y-m-d");
		$lastWeek = $date->modify("-7 days");
		$lastWeek = $lastWeek->format("Y-m-d H:i:s");
		$lastMonth = $date->modify("-1 months");
		$lastMonth = $lastMonth->format("Y-m-d H:i:s");


		// color optinos: .bg-red,.bg-yellow,.bg-aqua,.bg-blue,.bg-light-blue,.bg-green,.bg-navy,.bg-teal,.bg-olive,.bg-lime,.bg-orange,.bg-fuchsia,.bg-purple,.bg-maroon,.bg-black
		return array(
			'orders' => [
					'config' => [
						'color' => 'bg-red',
						'icon' => 'ion-ios-cart',
						'link' => []
					],
					'data' => [
						"new" => [
							'data' => Core::by('Order')->where('status','pending')->where("created_at",">=",$today)->count(),
							'link' => url('admin/panel/view/order/all?keyword='.date('Y-m-d')),
						],
						"outstanding" => [
							'data' => Core::by('Order')->where('status','pending')->count(),
							'link' => url('admin/panel/view/order/all?keyword=pending'),
						],
						"completed" => [
							'data' => Core::by('Order')->where('status','completed')->count(),
							'link' => url('admin/panel/view/order/all?keyword=completed'),
						]
					]
				],

				'products' => [
						'config' => [
							'color' => 'bg-yellow',
							'icon' => 'ion-bag',
							'link' => url('admin/panel/view/product/all')
						],
						'data' => [
							"total" => [
								'data' => Core::by('Product')->count(),
								'link' => url('admin/panel/view/product/all')
							]
						]
					],

				'clients' => [
						'config' => [
							'color' => 'bg-aqua',
							'icon' => 'ion-person-stalker',
							'link' => url('admin/panel/view/user/all')
						],
						'data' => [
							"total" => [
								'data' => Core::by('User')->count(),
								'link' => url('admin/panel/view/user/all')
							]
						]
					],


		);
	}//stats

}//Admin
