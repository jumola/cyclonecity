<!DOCTYPE html>
<html>
<head>
	<title>{{ config('config.app.name') }}</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name = "viewport" content = "initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no">

	<script src="{{asset('js/jquery.js')}}"></script>
	<script src="{{asset('js/bootstrap.js')}}"></script>

	<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('css/fonts.css')}}">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<link rel="stylesheet" href="{{asset('css/style-mobile.css')}}">

	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<script>
	var BaseUrl = "{{ URL::to('') }}/";
	var Facebook_id = "{{ config('config.facebook.api.app_id') }}";
</script>
<script src="{{ asset('js/main.js') }}"></script>
<body class="layout row">
	<div id="fb-root"></div>
	<div class="wrapper row">
		@yield("section")
	</div>
</body>
</html>
