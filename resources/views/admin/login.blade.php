<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ $config['app']['name'] }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset('admin/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset('admin/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="{{ asset('admin/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a><b>{{ $config['app']['name'] }}</b></a>
      </div><!-- /.login-logo -->


      <div class="login-box-body">

        @include('admin.sections.alert-response')

      @if( $_request->input('forgot') )



        	<p class="login-box-msg">Password Recovery</p>
        	<form action="{{ URL::to('admin/panel/forgot-password') }}" method="POST">
          		<input type='hidden' name='_token' value='{{ csrf_token() }}' />
          		<div class="form-group has-feedback">
            		<input name="email" type="email" class="form-control" placeholder="Your Email" required/>
            		<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          		</div>

          		<div class="row">
            		<div class="col-xs-12 text-center">
              			<button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                    <br />
                    <a href="{{ url('admin/panel') }}">
                      <p >Login</p>
                    </a>
            		</div><!-- /.col -->
          		</div>
        	</form>

      @else

        	<p class="login-box-msg">Enter Email & Password</p>
        	<form action="{{ URL::to('admin/panel/login') }}" method="POST">
          		<input type='hidden' name='_token' value='{{ csrf_token() }}' />
          		<div class="form-group has-feedback">
            		<input name="email" type="email" class="form-control" placeholder="Your Email" required/>
            		<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          		</div>
              <div class="form-group has-feedback">
            		<input name="password" type="password" class="form-control" placeholder="Your Password" required/>
            		<span class="glyphicon glyphicon-lock form-control-feedback"></span>
          		</div>
          		<div class="row">
            		<div class="col-xs-12 text-center">
              			<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    <br />
                    <a href="{{ url('admin/panel?forgot=true') }}">
                      <p >Forgot Your Password?</p>
                    </a>
            		</div><!-- /.col -->
          		</div>
        	</form>

      @endIf

      	</div><!-- /.login-box-body -->

    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('admin/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset('admin/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="{{ asset('admin/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
</body>
</html>
