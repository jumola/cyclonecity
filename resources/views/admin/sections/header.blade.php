<header class="main-header">
    <!-- Logo -->
    <a href="{{ url('admin/panel') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">{{ config("config.app.name") }}</span>
    </a>

	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
  		<!-- Sidebar toggle button-->
  		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
    		<span class="sr-only">Toggle navigation</span>
  		</a>
  		<div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              	<!-- Messages: style can be found in dropdown.less-->
              		<!-- @admin/sections/header/messages -->
              	<!-- Notifications: style can be found in dropdown.less -->
              		<!-- @admin/sections/header/notifications -->
              	<!-- Tasks: style can be found in dropdown.less -->
              		<!-- @admin/sections/header/tasks -->
              	<!-- User Account: style can be found in dropdown.less -->
              		@include("admin/sections/header/user-menu")
              	<!-- Control Sidebar Toggle Button -->
              		<!-- @admin/sections/header/toggle-menu -->
            </ul>
  		</div>
	</nav>
</header>
