<div class="user-panel">
	<div class="pull-left image">
		<img src="{{ asset('admin/dist/img/avatar5.png') }}" class="img-circle" alt="User Image" />
	</div>
	<div class="pull-left info">
		<p>{{ $admin->first_name }} {{ $admin->last_name }}</p>
		<a href="#">
			<i class="fa fa-circle text-success"></i>
			Online
		</a>
	</div>
</div>
