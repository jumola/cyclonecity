<ul class="sidebar-menu">

    <li class="header">MAIN NAVIGATION</li>

    <li class="treeview">
      <a href="{{ url('admin/panel') }}">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
      </a>
    </li>
    <li class="treeview">
      <a href="{{ url('admin/panel/view/order/all') }}">
        <i class="fa fa-table"></i> <span>Orders</span></i>
      </a>
    </li>
    <li class="treeview">
      <a href="{{ url('admin/panel/view/product/all') }}">
        <i class="fa fa-table"></i> <span>Products</span></i>
      </a>
    </li>
    <li class="treeview">
      <a href="{{ url('admin/panel/view/user/all') }}">
        <i class="fa fa-table"></i> <span>Clients</span></i>
      </a>
    </li>

    @if($admin->super == 'yes' )
    <li class="treeview">
      <a href="{{ url('admin/panel/view/admin/all') }}">
        <i class="fa fa-table"></i> <span>Admins</span></i>
      </a>
    </li>
    @endIf

</ul>
