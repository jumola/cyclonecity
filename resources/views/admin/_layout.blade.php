<!DOCTYPE html>
<html>
<head>
	<title>{{ $config['app']['name'] }}</title>
    @include("admin/sections/asset-files-css")
		@include("admin/sections/asset-files-js")

</head>
<script>
var BaseUrlRoot = '{{ url('') }}/';
</script>
<body class="skin-blue sidebar-mini">
	@if(session()->get("admin.in"))
		<div class="wrapper">
	  		@include("admin/sections/header")
	  		<!-- Left side column. contains the logo and sidebar -->
		  	<aside class="main-sidebar">
		    	<!-- sidebar: style can be found in sidebar.less -->
		    	<section class="sidebar">
		      		<!-- Sidebar user panel -->
		      		@include("admin/sections/sidebar/user-panel")
		      		<!-- sidebar menu: : style can be found in sidebar.less -->
		      		@include("admin/sections/sidebar/tabs")
		    	</section>
		    	<!-- /.sidebar -->
		  	</aside>

	  			@yield("section")

	  		@include("admin/sections/footer")
	  		<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
	  		<div class='control-sidebar-bg'></div>
		</div><!-- ./wrapper -->
	@endIf
</body>
</html>
