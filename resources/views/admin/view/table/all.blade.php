@extends("admin/_layout")
@section("section")
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">

				<ol class="breadcrumb box-title">
					<li><a href="{{ url('admin/panel/view/'. $table .'/all') }}">{{ ucwords(str_replace('_',' ',altStrings($table))) }}</a></li>
					<li class="active">All</li>
				</ol>

				<div class="box-tools">
					<div class="input-group">
						<a href="{{ url('admin/panel/view/'. $table .'/add') }}">
							<button class="btn btn-success">Add</button>
						</a>
					</div>

				</div>


			</div><!-- /.box-header -->

			<div class="box-body table-responsive">
				<table id="example1" class="table table-hover table-striped table-bordered">
					<thead>
						<tr>
							@if( $table == 'user' )
							<th>Tier</th>
							@endIf
							@foreach($columns as $column)
								<th>{{ ucwords(str_replace('_',' ',$column)) }}</th>
							@endforeach
							<th>Actions</th>
						</tr>

					</thead>
					<tbody>
						@foreach( $getData as $data )

						<tr data-id="{{ $data->id }}">
							@if( $table == 'user' )
							<td >
								<div class="ui fluid search selection dropdown">
									<input name="tier" type="hidden" value="{{ $data->tier }}" class="fxn-change-tier">
									<i class="dropdown icon"></i>
									<div class="default text">Select</div>
									<div class="menu">
										@foreach( getEnumValues('user','tier')  as $tier )
										<div class="item" data-value="{{ $tier }}">{{ ucwords(str_replace('_',' ',$tier)) }}</div>
										@endforeach
									</div>
								</div>
							</td>
							@endIf

							@foreach($columns as $column)
							   @if( $table == 'product' )
								   @if( in_array($column,['regular_price','sale_price','tier_1','tier_2','tier_3','tier_4','tier_5']) )
									 <td><input name="{{ $column }}" class="form-control fxn-change-product-tier" type="number" value="{{ $data->$column }}" style="width: 80px"></td>
									 @else
									 <td><small><?= $data->$column ?></small></td>
									 @endIf
								 @else
								 <td><small><?= $data->$column ?></small></td>
								 @endIf
							@endforeach


							<td>
								<button class="btn btn-xs btn-primary" onclick="window.location = '{{ URL::to('admin/panel/view/'. $table .'/edit?id='.$data->id) }}'">View</button>
								<a href="{{ URL::to('admin/panel/view/'. $table .'/delete?id='.$data->id) }}" onclick="return confirm('Are your sure?');">
									<button class="btn btn-xs btn-danger">Delete</button>
								</a>
							</td>
						</tr>

						@endforeach
					</tbody>
				</table>
			</div><!-- /.box-body -->
		</div><!-- /.box -->

	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>


<input name="param_keyword" type="hidden" value="{{ $_request->input('keyword') }}" />
<input name="_token" type="hidden" value="{{ csrf_token() }}" />
<script type="text/javascript">
	$(function () {
		$('.ui.dropdown').dropdown();
		var dataTable = $("#example1").dataTable( {

		});
		dataTable.fnFilter( $('[name=param_keyword]').val() );


		$('.fxn-change-tier').change(function(){
			var parent = $(this).closest('tr');
			var newTier = $(this).val();
			var user_id = parent.attr('data-id');
			$.post(BaseUrlRoot + 'save-user-tier',{
				_token: $('[name=_token]').val(),
				id: user_id,
				tier: newTier
			},function(response){

			});
		});

		$('.fxn-change-product-tier').change(function(){
			var columns = ['regular_price','sale_price','tier_1','tier_2','tier_3','tier_4','tier_5'];
			var parent = $(this).closest('tr');
			var id = parent.attr('data-id');

			var data = {
				_token: $('[name=_token]').val(),
				id: id
			};
			$.each( columns , function(i){
				var column = columns[i];
				data[column] = $("[name="+ column +"]").val();
			});

			$.post(BaseUrlRoot + 'save-product-pricing',data,function(response){});
		});

		// $('#example2').dataTable({
		// 	"bPaginate": true,
		// 	"bLengthChange": false,
		// 	"bFilter": false,
		// 	"bSort": true,
		// 	"bInfo": true,
		// 	"bAutoWidth": false
		// });

	});
</script>

@stop
