@extends("admin/_layout")
@section("section")
<!-- Content Wrapper. Contains page content -->
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>


<div class="content-wrapper">

    <!-- Main content -->
	<section class="content" style="height: 100%; min-height: 100%; overflow: hidden;">

    	<div class="col-sm-10 col-md-7">

				@include('admin.view.alert-response')

				<div class="box box-primary">

					<div class="box-header">
						<h3 class="box-title">{{ ucwords(str_replace('_',' ',altStrings($table))) }} > Create</h3>
					</div><!-- /.box-header -->

					<!-- form start -->
					<form method="POST" action="{{ URL::to('admin/panel/view/'.$table.'/create') }}" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{ csrf_token() }}" />

						<div class="box-body">

							@foreach($columns as $column)
								<div class="form-group">
									<label for="field{{ $column }}">{{ ucwords(str_replace('_',' ',$column))  }}</label>
				    				@if( isset($inputs[$table]) && isset($inputs[$table][$column]) )

				    					@if(key($inputs[$table][$column]) == "file")
												<input name="{{ $column }}" type="file" id="field{{ $column }}" value="{{ old($column) }}">
				    					@elseif(key($inputs[$table][$column]) == "checkbox")
				    						<input name="{{ $column }}" type="checkbox"  id="field{{ $column }}"/>
				    						<br />
				    					@elseIf(key($inputs[$table][$column]) == "select")
												<div class="ui fluid search selection dropdown {{ isset($params[$column]) ? 'disabled' : '' }}">
													<input type="hidden" name="{{ $column }}" value="{{ isset($params[$column]) ? $params[$column] : old($column) }}">
													<i class="dropdown icon"></i>
													<div class="default text">Select Option</div>
													<div class="menu">
														<!-- if select array options is already provided -->
														@if( $inputs[$table][$column]['select'] == 'enum')
															@foreach( getEnumValues($table,$column) as $option)
																<div class="item" data-value="{{ $option }}">{{ ucwords(str_replace('_',' - ',$option)) }}</div>
															@endforeach
														@elseif( in_array($inputs[$table][$column]['select'],['user','author']) )
															@foreach( getSelectOptions( $inputs[$table][$column]['select'] ) as $option)
																<div class="item" data-value="{{ $option->id }}">{{ $option->first_name }} {{ $option->last_name }}</div>
															@endforeach
														@elseif( is_array($inputs[$table][$column]['select']) )
															@foreach( $inputs[$table][$column]['select'] as $option)
																<div class="item" data-value="{{ $option }}">{{ $option }}</div>
															@endforeach
														@else
															@foreach( getSelectOptions( $inputs[$table][$column]['select'] ) as $option)
																<div class="item" data-value="{{ $option->id }}">{{ $option->name }}</div>
															@endforeach
														@endIf
													</div>
												</div>

											@elseIf(key($inputs[$table][$column]) == "editor")
												<textarea id="editor-{{ $column }}" name="{{ $column }}" rows="20" cols="80"></textarea>
												<script type="text/javascript">
													$(function () { CKEDITOR.replace('editor-{{ $column }}'); CKEDITOR.config.allowedContent = true; $(".textarea").wysihtml5(); });
												</script>

											@elseIf( key($inputs[$table][$column]) == "codeEditor" )
												<input name="{{ $column }}" type="hidden" value="{{ old($column) }}" />
												<div style="height: 300px; position: relative;overflow: hidden;background-color: #272822;">
													<div style="
														    position: absolute;
														    top: 0;
														    right: 0;
														    bottom: 0;
														    left: 0;
														    z-index: 5;
														    overflow-y: auto;
														    overflow-x: hidden;
														">
														<div id="editor-{{ $column }}" style="width: 100%" class="editor-master"></div>
													</div>
												</div>
												<script>
													// init learning
													var editor{{ $column }} = ace.edit("editor-{{ $column }}");
													// init editor
													editor{{ $column }}.setTheme("ace/theme/tomorrow_night");
													editor{{ $column }}.getSession().setMode("ace/mode/php");
													editor{{ $column }}.getSession().setTabSize(3);
													editor{{ $column }}.getSession().setUseWrapMode(false);
													editor{{ $column }}.getSession().on('change', function(){
													  $('[name="{{ $column }}"]').val( editor{{ $column }}.getSession().getValue());
													});
													editor{{ $column }}.setOptions({
														enableBasicAutocompletion: true,
												   	enableLiveAutocompletion: true,
													});
												</script>

				    					@endIf

				    				@else
											<input name="{{ $column }}" type="text" class="form-control input-sm" id="field{{ $column }}" placeholder="{{ $column }}" value="{{ old($column) }}" >
				    				@endIf
								</div>
		    			@endforeach

						</div><!-- /.box-body -->

						<div class="box-footer">
							<button class="btn btn-success">Create Now</button>
						</div>
					</form>
				</div><!-- /.box -->





    	</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
	$('.ui.dropdown').dropdown();
</script>

<style>
.editor-master {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    font-size:16.5px;
}
.ace_editor, .ace_editor *{
    font-family: "Monaco", "Menlo", "Ubuntu Mono", "Droid Sans Mono", "Consolas", monospace !important;
}
</style>
@stop
