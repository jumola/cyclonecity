@extends("admin/_layout")
@section("section")

<script src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('admin/ckeditor/adapters/jquery.js') }}"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
	<section class="content" style="height: 100%; min-height: 100%; overflow: hidden;">

    	<div class="col-sm-6 col-md-6">

				@include('admin.view.alert-response')

				<div class="box box-primary">

					<div class="box-header">

						<ol class="breadcrumb box-title">
							<li><a href="{{ url('admin/panel/view/'. $table .'/all') }}">{{ ucwords(str_replace('_',' ',altStrings($table)))    }}</a></li>
							<li class="active">ID: {{  $target['id'] }}</li>
						</ol>

					</div>

	    		<form method="POST" action="{{ URL::to('admin/panel/view/'.$table.'/update') }}" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{ csrf_token() }}" />
						<input name="id" type="hidden" value="{{ $target['id'] }}" />

						<div class="box-body">
		    			@foreach($columns as $column)
		    				<span><strong>{{ ucwords(str_replace('_',' ',$column))   }}</strong></span>

		    				@if( isset($inputs[$table]) && isset($inputs[$table][$column]) )

									@if(key($inputs[$table][$column]) == "file")
		    						<input name="{{ $column }}" type="file" class="form-control input-sm"/>
		    						<?php $img = "assets/". $table . "/" . $target['id'] . ".png"; ?>
		    						@if( File::exists( public_path($img) ) )
		    							<p>current {{ $column }} preview</p>
		    							<img height="50" src="{{ asset($img) }}" />
		    							<br />
		    						@endIf


		    					@elseIf(key($inputs[$table][$column]) == "select")
										<div class="ui fluid search selection dropdown">
											<input type="hidden" name="{{ $column }}" value="{{ $target[$column] }}">
											<i class="dropdown icon"></i>
											<div class="default text">Select Option</div>
											<div class="menu">

												<!-- if select array options is already provided -->
												@if( $inputs[$table][$column]['select'] == 'enum')
													@foreach( getEnumValues($table,$column) as $option)
														<div class="item" data-value="{{ $option }}">{{ ucfirst($option) }}</div>
													@endforeach
												@elseif( in_array($inputs[$table][$column]['select'],['user','author']) )
													@foreach( getSelectOptions( $inputs[$table][$column]['select'] ) as $option)
														<div class="item" data-value="{{ $option->id }}">{{ $option->first_name }} {{ $option->last_name }}</div>
													@endforeach
												@elseif( is_array($inputs[$table][$column]['select']) )
													@foreach( $inputs[$table][$column]['select'] as $option)
														<div class="item" data-value="{{ $option }}">{{ $option }}</div>
													@endforeach
												@else
													@foreach( getSelectOptions( $inputs[$table][$column]['select'] ) as $option)
														<div class="item" data-value="{{ $option->id }}">{{ $option->name }}</div>
													@endforeach
												@endIf


											</div>
										</div>

									@elseIf(key($inputs[$table][$column]) == "editor")
										<textarea id="editor-{{ $column }}" name="{{ $column }}" rows="20" cols="80">{{ htmlspecialchars($target[$column])  }}</textarea>
										<script type="text/javascript">
											$(function () {
												CKEDITOR.replace('editor-{{ $column }}',{
													filebrowserUploadUrl : '{{ URL::to('image-uploader') }}',
													height: '300px'
												});
												CKEDITOR.config.allowedContent = true;
												// CKEDITOR.config.startupMode = 'source';
												// $("#editor-{{ $column }}").wysihtml5();
												// CKEDITOR.instances['editor-{{ $column }}'].setData('');
											});
										</script>

									@elseIf(key($inputs[$table][$column]) == "codeEditor")
										<input name="{{ $column }}" type="hidden" value="{{ $target[$column] }}" />
										<div style="height: 300px; position: relative;overflow: hidden;background-color: #272822;">
											<div style="
												    position: absolute;
												    top: 0;
												    right: 0;
												    bottom: 0;
												    left: 0;
												    z-index: 5;
												    overflow-y: auto;
												    overflow-x: hidden;
												">
												<div id="editor-{{ $column }}" style="width: 100%" class="editor-master">{{ $target[$column]  }}</div>
											</div>
										</div>
										<script>
											// init learning
											var editor{{ $column }} = ace.edit("editor-{{ $column }}");
											// init editor
											editor{{ $column }}.setTheme("ace/theme/tomorrow_night");
											editor{{ $column }}.getSession().setMode("ace/mode/php");
											editor{{ $column }}.getSession().setTabSize(3);
											editor{{ $column }}.getSession().setUseWrapMode(false);
											editor{{ $column }}.getSession().on('change', function(){
											  $('[name="{{ $column }}"]').val( editor{{ $column }}.getSession().getValue());
											});
											editor{{ $column }}.setOptions({
												enableBasicAutocompletion: true,
										   	enableLiveAutocompletion: true,
											});
										</script>

		    					@endIf


		    				@else
		    					<input name="{{ $column }}" type="text" class="form-control input-sm" value="{{ $target[$column] }}" />
		    				@endIf

		    			@endforeach
						</div>

						<div class="box-footer">
							<button class="btn btn-block btn-success">Save</button>
						</div>

	    		</form>

    	</div>
		</div>

		<!-- right -->
		 <div class="col-sm-6 col-md-6">
			 @if( $table == 'order' )
			 <div class="box">
	 			<div class="box-header">
	 				<ol class="breadcrumb box-title">
	 					<li class="active">Products</li>
	 				</ol>
	 			</div><!-- /.box-header -->

	 			<div class="box-body table-responsive">
	 				<table id="example1" class="table table-hover table-striped table-bordered">
	 					<thead>
	 						<tr>
	 							<th>Name</th>
	 							<th>Quantity</th>
	 							<th>Total</th>
	 						</tr>
	 					</thead>
	 					<tbody>
	 						@foreach( $orderProducts as $item )
	 						<tr onclick="window.location = '{{ URL::to('admin/panel/view/product/edit?id='.$item->id) }}'" class="cursor-pointer">
	 							<td><small>{{ $item->product->name }}</small></td>
	 							<td><small>{{ $item->quantity }}</small></td>
	 							<td><small>${{ $item->total }}</small></td>
	 						</tr>
	 						@endforeach
	 					</tbody>
	 				</table>
	 			</div><!-- /.box-body -->
	 		</div><!-- /.box -->
			 @elseif( $table == 'user' )
			 <div class="box">
	 			<div class="box-header">
	 				<ol class="breadcrumb box-title">
	 					<li class="active">Actions</li>
	 				</ol>
	 			</div><!-- /.box-header -->

	 			<div class="box-body">
					<a href="http://cleaningsuppliesnt.com/wp-admin/user-edit.php?user_id={{ $target['wp_id'] }}&wp_http_referer=%2Fwp-admin%2Fusers.php" target="_blank">
						<button class="btn btn-block btn-success">Generate New Password</button>
					</a>

					<br />

					<a href="http://cleaningsuppliesnt.com/wp-admin/users.php?action=resend_welcome_email&user_id={{ $target['wp_id'] }}" target="_blank">
						<button class="btn btn-block btn-success">Resnd Welcome Email w/ Username & Password Reset Link</button>
					</a>



	 				<p><small>You need to login to the store (cleaningsuppliesnt.com/admin) for these functions.</small></p>
	 			</div><!-- /.box-body -->
	 		</div><!-- /.box -->
			 @endIf
		 </div>


	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
	$('.ui.dropdown').dropdown();
</script>

<style>
.editor-master {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    font-size:16.5px;
}
.ace_editor, .ace_editor *{
    font-family: "Monaco", "Menlo", "Ubuntu Mono", "Droid Sans Mono", "Consolas", monospace !important;
}
</style>
@stop
