@extends("admin/_layout")
@section("section")
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-header">

							<div class="box-tools">
								<!-- <div class="input-group">
									<input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
									<div class="input-group-btn">
										<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
									</div>
								</div> -->
								<a href="{{ url('admin/panel/view/course/add') }}">
									<button class="btn btn-sm btn-success">Create a Course</button>
								</a>
								
							</div>

						</div><!-- /.box-header -->
						<div class="box-body overflow-visible">
							<p>This is a list of your course.</p>

							@include('admin.view.alert-response')




						</div><!-- /.box-body -->
					</div><!-- /.box -->
				</div>
			</div>
		</section><!-- /.content -->



</div><!-- /.content-wrapper -->
@stop
