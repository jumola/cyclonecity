@if( session('error') || session('success') )

<div class="padding-top">
	@if( session('error') )
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong>Oops!</strong>
		<div>
			{!! session('error') !!}
		</div>
	</div>
	@endIf

	@if( session('success') )
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong>Success!</strong>
		<div>
			{!! session('success') !!}
		</div>
	</div>
	@endIf

</div>
@endif
