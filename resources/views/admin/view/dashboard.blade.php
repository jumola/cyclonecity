@extends("admin/_layout")
@section("section")
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
    	<section class="content-header">
		      <h1>Dashboard </h1>
    	</section>


	<!-- Main content -->
	<section class="content">
		@foreach($adminStats as $table => $stat )
    		<div class="row">
    			@foreach( $stat['data'] as $name => $value )
      			<div class="col-lg-4 col-sm-4 col-xs-6 cursor-pointer" onclick="window.location = '{{ $value['link'] }}' ">
      				<div class="small-box {{ $stat['config']['color'] }}">
                <div class="inner">
                  <h3>{{ $value['data'] }}</h3>
                  <p><strong>{{ strtoupper($table) }}</strong>: {{ strtoupper( str_replace("_"," ",$name) ) }}</p>
                </div>
                <div class="icon padding-top">
                  <small><i class="ion {{ $stat['config']['icon'] }}"></i></small>
                </div>
                <a class="small-box-footer">&nbsp;</a>
      				</div>
      			</div><!-- ./col -->
    			@endforeach
    		</div>
  	@endforeach
    </section><!-- /.content -->

</div><!-- /.content-wrapper -->
@stop
