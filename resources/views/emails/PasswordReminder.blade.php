<!DOCTYPE html>
<html>
<body>

	<h2>Hi {{ $params['admin']->first_name }}.</h2>

  <p>You requested for your password to {{ config('config.app.name') }}.</p>

	<p>Your password is: <strong>{{ $params['admin']->password }}</strong></p>

	<p>You may now log back in using this password.</p>

	<a href="{{ url('admin/panel') }}">
		<h1>CLICK HERE TO LOGIN</h1>
	</a>

</body>
</html>
