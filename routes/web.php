<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");


Route::get('', 'ViewController@view');
Route::get('login-facebook', 'ViewController@loginViaFacebook');
Route::get('login-facebook-cb', 'ViewController@loginViaFacebookCb');
Route::get('logout', "ViewController@page");
Route::get('importer', "ViewController@importer");


//AdminController
Route::get('admin/panel', "AdminController@admin");
Route::get('admin/panel/app-reset', "AdminController@reset_app");
Route::any('admin/panel/custom/{view}', "AdminController@custom");
Route::get('admin/panel/download/{table}', "AdminController@download");
Route::get('admin/panel/logout', "AdminController@logout");
Route::any('admin/panel/view/{table}/{view}', "AdminController@view");
Route::post('admin/panel/login', "AdminController@login");
Route::post('admin/panel/forgot-password', "AdminController@passwordReminder");


Route::any('save-product-pricing', "AdminController@saveProductPricing");
Route::any('save-user-tier', "ApiController@saveUserTier");
