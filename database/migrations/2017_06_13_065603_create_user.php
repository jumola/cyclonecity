<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create("user",function($table){
        $table->increments("id");

        $table->integer("contacttype_id")->nullable();

        $table->string("wp_id")->nullable();
        $table->string("email")->nullable();
        $table->string("name")->nullable();
        $table->string("username")->nullable();
        $table->string("password")->nullable();



        $table->string("job_title")->nullable();
        $table->string("business_name")->nullable();
        $table->string("work_phone")->nullable();
        $table->string("mobile_phone")->nullable();
        $table->string("address1")->nullable();
        $table->string("address2")->nullable();
        $table->string("suburb")->nullable();
        $table->string("state")->nullable();
        $table->string("postal")->nullable();

        $table->string("referrer")->nullable();
        $table->string("group_type")->nullable();
        $table->string("profile_image")->nullable();
        $table->string("user_level")->nullable();

        $table->timeStamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user');
    }
}
