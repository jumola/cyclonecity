<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderproduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create("orderproduct",function($table){
        $table->increments("id");

        $table->integer("order_id");

        $table->string("wp_id")->nullable();

        $table->string("status")->nullable();
        $table->string("currency")->nullable();
        $table->float("total")->nullable();
        $table->longText("customer_note")->nullable();
        $table->string("payment_method_title")->nullable();
        $table->string("transaction_id")->nullable();

        $table->timeStamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
