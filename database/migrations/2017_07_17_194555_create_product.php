<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create("product",function($table){
        $table->increments("id");

        $table->string("wp_id")->nullable();

        $table->string("name")->nullable();
        $table->longText("description")->nullable();
        $table->longText("short_description")->nullable();

        $table->float("price")->nullable();
        $table->float("regular_price")->nullable();
        $table->float("sale_price")->nullable();

        $table->longText("image")->nullable();

        $table->timeStamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product');
    }
}
