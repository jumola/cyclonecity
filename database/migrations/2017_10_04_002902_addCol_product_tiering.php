<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColProductTiering extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product',function($table){
          $table->float('tier_1')->nullable()->after('sale_price');
          $table->float('tier_2')->nullable()->after('tier_1');
          $table->float('tier_3')->nullable()->after('tier_2');
          $table->float('tier_4')->nullable()->after('tier_3');
          $table->float('tier_5')->nullable()->after('tier_4');
        });

        Schema::table('user',function($table){
          $table->enum('tier',['default','tier_1','tier_2','tier_3','tier_4','tier_5'])->after('wp_id')->default('default');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
